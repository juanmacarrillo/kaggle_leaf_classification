#### Leaf classification for the kaggle challenge
# https://www.kaggle.com/c/leaf-classification

#Load libraries
library (data.table)
library (randomForest)
library (e1071)
library (dplyr)
library (party)


#Load data
train <- fread ("train.csv")
train <- train [,-1]
test <- fread ("test.csv")

#Try to reduce the number of variables to reduce load and reduce overfitting
#From the summary of each PCA, select the variables that gather about the 80% of cumulative 
## proportion using the score
train_margin <- select (train, contains("margin"))
margin_PCA <- princomp (train_margin)
summary (margin_PCA)
margin_PCA_scores <- margin_PCA$scores [,1:8]

train_shape <- select (train, contains ("shape"))
shape_PCA <- princomp (train_shape)
summary (shape_PCA)
shape_PCA_scores <- shape_PCA$scores [,1:2]

train_texture <- select (train, contains ("texture"))
texture_PCA <- princomp (train_texture)
summary (texture_PCA)
texture_PCA_scores <- texture_PCA$scores [,1:13]

# So now we have the three features reduced to 23 components.
# I create a new train dataframe with every component.
train_PCA <- data.frame (train$species, margin_PCA_scores, shape_PCA_scores, texture_PCA_scores)
colnames (train_PCA) <- c("species", "Comp1", "Comp2", "Comp3", "Comp4", "Comp5", "Comp6",
"Comp7", "Comp8", "Comp9", "Comp10", "Comp11", "Comp12", "Comp13", "Comp14", "Comp15", "Comp16", "Comp17",
"Comp18", "Comp19", "Comp20", "Comp21", "Comp22", "Comp23")

# I could apply PCA to the test file, but because they are different data, I couldn't apply any
# training from the train file to the test file.

# I need to apply the same PCA rules from the train_PCA to a new test_PCA
test_margin_PCA <- predict(margin_PCA, test %>% select (contains("margin")))[,1:8]
test_shape_PCA <- predict (shape_PCA, test %>% select (contains ("shape")))[,1:2]
test_texture_PCA <- predict (texture_PCA, test %>% select (contains ("texture")))[,1:13]
# And create a new test data frame with the three features together
test_PCA <- data.frame (test_margin_PCA, test_shape_PCA, test_texture_PCA)
colnames (test_PCA) <- c("Comp1", "Comp2", "Comp3", "Comp4", "Comp5", "Comp6",
                         "Comp7", "Comp8", "Comp9", "Comp10", "Comp11", "Comp12", "Comp13", "Comp14", "Comp15", "Comp16", "Comp17",
                         "Comp18", "Comp19", "Comp20", "Comp21", "Comp22", "Comp23")

### First model to try, e1071::naiveBayes
naiveB <- naiveBayes(species ~., train_PCA)
naiveB_test <- predict (naiveB, newdata = test_PCA, type="raw")
naiveB_test <- data.frame (id=test$id, naiveB_test)
write.csv(naiveB_test, "naiveB_test.csv", row.names = F)
#scores in LB, 0.56294

## Second, I will try with randomforest
leaf_forest <- randomForest (species ~., data=train_PCA,
                             mtry=3, importance=T)
forest_test <- predict (leaf_forest, newdata = test_PCA, type="prob")
fores_test <- data.frame (id=test$id, forest_test)
write.csv(fores_test, "forest_test.csv", row.names = F)

#############
# with no PCA
############
naiveB_noPCA <- naiveBayes(species ~., train)
naiveB_test_noPCA <- predict (naiveB_noPCA, newdata = test, type="raw")
naiveB_test_noPCA <- data.frame (id=test$id, naiveB_test_noPCA)
write.csv(naiveB_test_noPCA, "naiveB_test_noPCA.csv", row.names = F)
## Score in LB of 1.53, way worse than with no PCA